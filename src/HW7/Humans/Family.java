package HW7.Humans;

import HW7.Pets.Pet;

import java.util.*;

public class Family implements HumanCreator {
    private Women mother;
    private Man father;
    private List<Human> childrens;
    private final Set<Pet> pets = new HashSet<>();


    static {
        System.out.printf("New Class is loading %s\n", Family.class);
    }

    {
        System.out.printf("New Object is loading %s\n", Family.class);
    }


    public Family(Women mother, Man father) {
        this.mother = mother;
        this.father = father;
        this.childrens = new ArrayList<>();
        mother.setFamily(this);
        father.setFamily(this);
    }

    public Family(Women mother, Man father, Pet p) {
        this(mother, father);
        pets.add(p);
    }


    public Human getMother() {
        return mother;
    }

    public void setMother(Women mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Man father) {
        this.father = father;
    }

    public Set<Pet> getPets() {
        return pets;
    }

    public void addPet(Pet pet) {
        pets.add(pet);
    }

    public List<Human> getChildrens() {
        return childrens;
    }

    public void setChildrens(List<Human> childrens) {
        this.childrens = childrens;
    }


    public void addChild(Human child) {
        childrens.add(child);
        child.setFamily(this);
    }

    public boolean deleteChild(int index) {

        if (index < 0 || index > childrens.size()) return false;

        childrens.remove(index);

        return true;
    }

    public int countFamily() {
        return 2 + childrens.size();
    }

    public String showChilderens() {
        StringJoiner sj = new StringJoiner(", ");
        for (Human child : childrens) {
            sj.add(String.format("Child = %s %s", child.getName(), child.getSurname()));
        }

        return sj.toString();
    }

    @Override
    public String toString() {
        return String.format("Mother=%s %s, Father=%s %s, childerns=[%s]",
                mother.getName(),
                mother.getSurname(),
                father.getName(),
                father.getSurname(),
                showChilderens()
        );
    }

    @Override
    public boolean equals(Object obj){
        if (this == obj)
            return true;
        if (!(obj instanceof Family))
            return false;
        Family f = (Family) obj;
        return this.mother.equals(f.mother) && this.father.equals(f.father) && this.pets.equals(f.pets);
    }

    @Override
    public int hashCode(){
        int code = 11;
        int k = 10;
        code = k*code + mother.hashCode() + father.hashCode();
        return code;
    }

    @Override
    public void finalize(){
        System.out.println(this);
    }
}
