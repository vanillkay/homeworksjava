package HW7.Humans;


import HW7.Pets.Pet;

import java.util.Map;

public class Man extends Human {
    public Man() {
        super();
    }


    public Man(String name, String surname, int year) {
        super(name, surname, year);
    }


    public Man(String name, String surname, int year, int iq, Map<String, String> schedule) {
        super(name, surname, year, iq, schedule);
    }

    @Override
    public void greetPet(){
        System.out.printf("Hello from man, %s !\n", ((Pet) super.getFamily().getPets().toArray()[0]).getNickname());
    }

    public void repairCar(){
        System.out.println("Ohhh no, i need o repair my car !");
    }
}
