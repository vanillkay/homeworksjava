package HW7.Humans;

import HW1.Game;
import HW7.Pets.Pet;

import java.util.*;

public abstract class Human {
    private String name;
    private String surname;
    private int year;
    private int iq;
    private Family family;
    private Map<String, String> schedule;


    static {
        System.out.printf("New class is loading %s\n", Human.class);
    }

    {
        System.out.printf("New Object is loading %s\n", Human.class);
    }


    Human() {
        name = "no name";
        surname = "no surname";
        year = 0;
        iq = 0;
        schedule = new HashMap<>();
    }


    public Human(String name, String surname, int year) {
        this();
        this.name = name;
        this.surname = surname;
        this.year = year;
    }


    public Human(String name, String surname, int year, int iq, Map<String, String> schedule) {
        this(name, surname, year);
        this.iq = iq;
        this.schedule = schedule;
    }

    public void greetPet() {
        System.out.printf("Hello %s !\n", ((Pet) family.getPets().toArray()[0]).getNickname());
    }

    public boolean feedPat(boolean isTimeToEat) {
        Set<Pet> p = family.getPets();
        if (isTimeToEat) {
            System.out.printf("Hm...I need to feed %s !\n", ((Pet) p.toArray()[0]).getNickname());
            return true;
        }
        int random = Game.randomNum(0, 101);
        if (((Pet) p.toArray()[0]).getTrickLevel() > random) {
            System.out.printf("Hm...I need to feed %s !\n", ((Pet) p.toArray()[0]).getNickname());
            return true;
        }
        System.out.printf("I think %s is not hungry !\n", ((Pet) p.toArray()[0]).getNickname());
        return false;
    }

    public void describePet() {
        System.out.printf("I have a %s, he is %d years old. He is %s ! \n",
                ((Pet) family.getPets().toArray()[0]).getSpecies(),
                ((Pet) family.getPets().toArray()[0]).getAge(),
                ((Pet) family.getPets().toArray()[0]).getTrickLevel() > 50 ? "very tricky" : "almost not tricky");
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public Map<String, String> getSchedule() {
        return schedule;
    }

    public void setSchedule(HashMap<String, String> schedule) {
        this.schedule = schedule;
    }


    public String showSchedule() {
        StringJoiner sj = new StringJoiner(", ");

        for (Map.Entry<String, String> entry : schedule.entrySet()) {
            sj.add(entry.getValue());
        }

        return sj.toString();
    }


    @Override
    public String toString() {
        return String.format("Human{name='%s', surname='%s', year=%d, iq=%d, schedule=[%s]}",
                getName(),
                getSurname(),
                getYear(),
                getIq(),
                showSchedule()
        );
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!(obj instanceof Human))
            return false;
        Human h = (Human) obj;
        if (this.year != h.year || this.iq != h.iq) return false;
        return this.name.equals(h.name) && this.surname.equals(h.surname);
    }

    @Override
    public int hashCode() {
        int code = 5;
        int k = 5;
        code = k * code + year + iq + name.hashCode();
        return code;
    }

    @Override
    public void finalize() {
        System.out.println(this);
    }
}
