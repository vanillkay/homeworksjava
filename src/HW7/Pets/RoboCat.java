package HW7.Pets;

import java.util.Set;

public class RoboCat extends Pet implements PetFoul {

    {
        super.setSpecies(Species.ROBOTCAT);
    }
    @Override
    public void respond() {
        System.out.println("Mr eovvvv, * trrrr");
    }

    @Override
    public void foul() {
        System.out.println("I need to break the computer");
    }

    public RoboCat(){
        super();
    }

    public RoboCat(String nickname){
        super(nickname);
    }

    public RoboCat(String nickname, int age, int trickLevel, Set<String> habits){
        super(nickname, age, trickLevel, habits);
    }
}
