package HW7;


import HW3.Planner;
import HW7.Humans.Family;
import HW7.Humans.Man;
import HW7.Humans.Women;
import HW7.Pets.DomesticCat;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class App {


    public static void main(String[] args) {


        Women h1 = new Women("Mary", "Gofran", 2003, 23, new HashMap<>());
        Man h2 = new Man("Mike", "Leviren", 2001);
        DomesticCat p2 = new DomesticCat("Vasya", 2, 43, new HashSet<String>() {{
            add("sleep");
            add("eat");
        }});

        Family f1 = new Family(h1, h2, p2);

        System.out.println(f1);
        f1.bornChild(f1, 2021);
        System.out.println(f1);
        h1.greetPet();
    }
}
