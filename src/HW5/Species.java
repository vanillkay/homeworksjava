package HW5;

public enum Species {
    UNKNOWN(false, 0, false),
    DOG(false, 4, true),
    CAT(false, 4, true),
    FISH(false, 0, false),
    BIRD(true, 2, true);
    final private boolean canFly;
    final private int numberOfLegs;
    final private boolean hasFur;

    Species(boolean canFly, int numberOfLegs, boolean hasFur) {
        this.canFly = canFly;
        this.numberOfLegs = numberOfLegs;
        this.hasFur = hasFur;
    }

    public boolean isCanFly() {
        return canFly;
    }

    public int getNumberOfLegs() {
        return numberOfLegs;
    }

    public boolean isHasFur() {
        return hasFur;
    }
}
