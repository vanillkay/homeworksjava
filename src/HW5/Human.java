package HW5;

import HW1.Game;

import java.util.Arrays;
import java.util.StringJoiner;

public class Human {
    private String name;
    private String surname;
    private int year;
    private int iq;
    private Family family;
    private String[][] schedule;


    static {
        System.out.printf("New class is loading %s\n", Human.class);
    }

    {
        System.out.printf("New Object is loading %s\n", Human.class);
    }

    public boolean feedPat(boolean isTimeToEat){
        Pet p = family.getPet();
        if (isTimeToEat){
            System.out.printf("Hm...I need to feed %s !\n", p.getNickname());
            return true;
        }
        int random = Game.randomNum(0, 101);
        if (p.getTrickLevel() > random){
            System.out.printf("Hm...I need to feed %s !\n", p.getNickname());
            return true;
        }
        System.out.printf("I think %s is not hungry !\n", p.getNickname());
        return false;
    }

    Human() {
        name = "no name";
        surname = "no surname";
        year = 0;
        iq = 0;
        schedule = new String[0][0];
    }


    public Human(String name, String surname, int year) {
        this();
        this.name = name;
        this.surname = surname;
        this.year = year;
    }



    public Human(String name, String surname, int year, int iq, String[][] schedule) {
        this(name, surname, year);
        this.iq = iq;
        this.schedule = schedule;
    }

    public void greedPet() {
        System.out.printf("Hello %s !\n", family.getPet().getNickname());
    }

    public void describePet() {
        System.out.printf("I have a %s, he is %d years old. He is %s ! \n",
                family.getPet().getSpecies(),
                family.getPet().getAge(),
                family.getPet().getTrickLevel() > 50 ? "very tricky" : "almost not tricky");
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public String[][] getSchedule() {
        return schedule;
    }

    public void setSchedule(String[][] schedule) {
        this.schedule = schedule;
    }


    public String showSchedule() {
        StringJoiner sj = new StringJoiner(", ");

        for (String[] day : schedule) {
            sj.add(Arrays.toString(day));
        }

        return sj.toString();
    }


    @Override
    public String toString() {
        return String.format("Human{name='%s', surname='%s', year=%d, iq=%d, schedule=[%s]}",
                getName(),
                getSurname(),
                getYear(),
                getIq(),
                showSchedule()
        );
    }

    @Override
    public boolean equals(Object obj){
        if (this == obj)
            return true;
        if (!(obj instanceof Human))
            return false;
        Human h = (Human) obj;
        if(this.year != h.year || this.iq != h.iq) return false;
        return this.name.equals(h.name) && this.surname.equals(h.surname);
    }

    @Override
    public int hashCode(){
        int code = 5;
        int k = 5;
        code = k*code + year + iq + name.hashCode();
        return code;
    }

    @Override
    public void finalize(){
        System.out.println(this);
    }
}
