package HW5;

import java.util.Arrays;
import java.util.StringJoiner;

public class Family {
    private Human mother;
    private Human father;
    private Human[] childrens;
    private Pet pet;


    static {
        System.out.printf("New Class is loading %s\n", Family.class);
    }

    {
        System.out.printf("New Object is loading %s\n", Family.class);
    }


    Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
        this.childrens = new Human[0];
        this.pet = new Pet();
    }

    public Family(Human mother, Human father, Pet p) {
        this.mother = mother;
        this.father = father;
        this.childrens = new Human[0];
        this.pet = p;
    }


    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public Human[] getChildrens() {
        return childrens;
    }

    public void setChildrens(Human[] childrens) {
        this.childrens = childrens;
    }


    public void addChild(Human child) {
        Human[] newChildrens = Arrays.copyOf(childrens, childrens.length + 1);
        newChildrens[newChildrens.length - 1] = child;
        child.setFamily(this);
        childrens = newChildrens;
    }

    public boolean deleteChild(int index) {

        if (index < 0 || index > childrens.length) return false;

        Human[] newChildrnes = new Human[childrens.length - 1];

        childrens[index].setFamily(new Family(new Human(), new Human()));

        Human[] newChildPart1 = Arrays.copyOfRange(childrens, 0, index);
        Human[] newChildPart2 = Arrays.copyOfRange(childrens, index + 1, childrens.length);

        System.arraycopy(newChildPart1, 0, newChildrnes, 0, newChildPart1.length);
        System.arraycopy(newChildPart2, 0, newChildrnes, newChildPart1.length, newChildPart2.length);

        childrens = newChildrnes;

        return true;
    }

    public int countFamily() {
        return 2 + childrens.length;
    }

    public String showChilderens() {
        StringJoiner sj = new StringJoiner(", ");
        for (Human child : childrens) {
            sj.add(String.format("Child = %s %s", child.getName(), child.getSurname()));
        }

        return sj.toString();
    }

    @Override
    public String toString() {
        return String.format("Mother=%s %s, Father=%s %s, childerns=[%s]",
                mother.getName(),
                mother.getSurname(),
                father.getName(),
                father.getSurname(),
                showChilderens()
        );
    }

    @Override
    public boolean equals(Object obj){
        if (this == obj)
            return true;
        if (!(obj instanceof Family))
            return false;
        Family f = (Family) obj;
        return this.mother.equals(f.mother) && this.father.equals(f.father) && this.pet.equals(f.pet);
    }

    @Override
    public int hashCode(){
        int code = 11;
        int k = 10;
        code = k*code + mother.hashCode() + father.hashCode();
        return code;
    }

    @Override
    public void finalize(){
        System.out.println(this);
    }
}
