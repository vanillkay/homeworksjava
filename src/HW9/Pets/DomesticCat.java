package HW9.Pets;

import java.util.Set;

public class DomesticCat extends Pet implements PetFoul {
    {
        super.setSpecies(Species.DOMESTICCAT);
    }


    @Override
    public void respond() {
        System.out.println("Meov...mrrrrr");
    }

    @Override
    public void foul() {
        System.out.println("I need to bite my owner");
    }

    public DomesticCat(){
        super();
    }

    public DomesticCat(String nickname){
        super(nickname);
    }

    public DomesticCat(String nickname, int age, int trickLevel, Set<String> habits){
        super(nickname, age, trickLevel, habits);
    }
}
