package HW9;


import HW9.Dao.FamilyController;
import HW9.Humans.Family;
import HW9.Humans.Man;
import HW9.Humans.Women;
import HW9.Pets.DomesticCat;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;

public class App {


    public static void main(String[] args) {


        LocalDate ld = LocalDate.now().minusYears(30).minusDays(23).minusMonths(9);
        LocalDateTime ldt = ld.atTime(LocalTime.now());
        long l = ldt.toInstant(ZoneOffset.ofHours(4)).toEpochMilli();

        Women h1 = new Women("Mary", "Gofran", l, 23, new HashMap<>());
        Man h2 = new Man("Mike", "Leviren", new Date().getTime() - 10000 * 7);
        DomesticCat p2 = new DomesticCat("Vasya", 2, 43, new HashSet<>() {{
            add("sleep");
            add("eat");
        }});

        System.out.println(h1);
        System.out.println(h1.describeAge());
    }
}
