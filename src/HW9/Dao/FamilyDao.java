package HW9.Dao;

import HW9.Humans.Family;

import java.util.List;

public interface FamilyDao {
    public List<Family> getAllFamilies();

    public Family getFamilyByIndex(int index);

    public boolean deleteFamily(int index);

    public boolean deleteFamily(Family f);

    public void saveFamily(Family f);




}
