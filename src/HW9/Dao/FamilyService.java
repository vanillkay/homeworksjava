package HW9.Dao;

import HW9.Humans.Family;
import HW9.Humans.Human;
import HW9.Pets.Pet;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

public class FamilyService {
    public FamilyDao db;


    FamilyService(){
        this.db = new CollectionFamilyDao();
    }

    FamilyService(FamilyDao db) {
        this.db = db;
    }

    public List<Family> getAllFamilies() {
        return db.getAllFamilies();
    }

    public void displayAllFamilies() {
        List<Family> f = db.getAllFamilies();

        for (int i = 0; i < f.size(); i++) {
            Family family = f.get(i);
            System.out.printf("Family number %d %n", i);
            System.out.println(family);
        }
    }

    public void getFamiliesBiggerThan(int num) {
        List<Family> f = new ArrayList<>(db.getAllFamilies());
        f.removeIf(x -> x.countFamily() <= num);
        for (int i = 0; i < f.size(); i++) {
            Family family = f.get(i);
            System.out.printf("Families bigger than %d %n", i);
            System.out.println(family);
        }
    }

    public void getFamiliesLessThan(int num) {
        List<Family> f = new ArrayList<>(db.getAllFamilies());
        f.removeIf(x -> x.countFamily() >= num);
        for (int i = 0; i < f.size(); i++) {
            Family family = f.get(i);
            System.out.printf("Families less than %d %n", i);
            System.out.println(family);
        }
    }

    public int countFamiliesWithMemberNumber(int num) {
        AtomicInteger count = new AtomicInteger();
        List<Family> f = db.getAllFamilies();

        f.forEach(x -> {
            if (x.countFamily() == num) {
                count.getAndIncrement();
            }
        });

        return count.intValue();
    }

    public void createNewFamily(Human h1, Human h2) {
        Family f1 = new Family(h1, h2);
        db.saveFamily(f1);
    }

    public void deleteFamilyByIndex(int index) {
        db.deleteFamily(index);
    }

    public Family bornChild(Family f, String fatherName, String motherName) {
        f.bornChild(f, 2001);
        db.saveFamily(f);
        return f;
    }

    public Family adoptChild(Family f, Human h) {
        f.addChild(h);
        db.saveFamily(f);
        return f;
    }

    public int count() {
        return db.getAllFamilies().size();
    }

    public Family getFamilyById(int index) {
        return db.getFamilyByIndex(index);
    }

    public Set<Pet> getPets(int index) {
        return db.getFamilyByIndex(index).getPets();
    }

    public void addPet(int index, Pet p) {
        Family f = db.getFamilyByIndex(index);
        f.addPet(p);
        db.saveFamily(f);
    }

}
