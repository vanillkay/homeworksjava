package HW9.Humans;

import HW1.Game;

import java.util.HashMap;

public interface HumanCreator {

    final String[] boyNames = new String[]{"Andrew", "Mike", "Jake"};
    final String[] girlsNames = new String[]{"Sophia", "Jane", "Olivia"};

    public default Human bornChild(Family family, int year) {
        int num = Game.randomNum(1, 10);
        int namePos = Game.randomNum(0, 2);
        if (num > 5) {
            Man boy = new Man(boyNames[namePos],
                    family.getFather().getSurname(), year, family.getFather().getIq() + family.getMother().getIq() / 2, new HashMap<>());
            boy.setFamily(family);
            family.addChild(boy);
            return boy;
        } else {
            Women girl = new Women(girlsNames[namePos],
                    family.getFather().getSurname(), year, family.getFather().getIq() + family.getMother().getIq() / 2, new HashMap<>());
            girl.setFamily(family);
            family.addChild(girl);
            return girl;
        }
    }

}
