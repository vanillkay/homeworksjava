package HW10;


import HW8.Dao.FamilyController;
import HW8.Humans.Family;
import HW8.Humans.Man;
import HW8.Humans.Women;
import HW8.Pets.DomesticCat;

import java.util.HashMap;
import java.util.HashSet;

public class App {


    public static void main(String[] args) {


        Women h1 = new Women("Mary", "Gofran", 2003, 23, new HashMap<>());
        Man h2 = new Man("Mike", "Leviren", 2001);
        DomesticCat p2 = new DomesticCat("Vasya", 2, 43, new HashSet<String>() {{
            add("sleep");
            add("eat");
        }});

        Women h3 = new Women("Sophia", "Jart", 2003, 23, new HashMap<>());
        Man h4 = new Man("Alex", "Grof", 2001);
        DomesticCat p3 = new DomesticCat("Myrka", 2, 43, new HashSet<String>() {{
            add("play");
            add("eat");
        }});


        FamilyController cont = new FamilyController();
        cont.createNewFamily(h1, h2);
        cont.createNewFamily(h3, h4);
        System.out.println(cont.count());
        System.out.println(cont.getAllFamilies());
        cont.displayAllFamilies();
        cont.addPet(0, p2);
        System.out.println(cont.getPets(0));
        System.out.println(cont.getFamilyById(1));
        Family f1 = cont.getFamilyById(0);
        System.out.println(cont.bornChild(f1, f1.getFather().getName(), f1.getMother().getName()));
        cont.getFamiliesBiggerThan(2);
        cont.getFamiliesLessThan(3);
        Family f2 = cont.getFamilyById(1);
        System.out.println(cont.getAllFamilies());
        Man h5 = new Man("Jake", "Miles", 2016);
        System.out.println(cont.adoptChild(f2, h5));
        System.out.println(cont.countFamiliesWithMemberNumber(3));
        cont.deleteFamilyByIndex(1);
        cont.deleteFamilyByIndex(0);
        System.out.println(cont.getAllFamilies());


    }
}
