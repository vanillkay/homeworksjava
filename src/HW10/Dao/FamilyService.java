package HW10.Dao;

import HW10.Humans.Family;
import HW10.Humans.Human;
import HW10.Pets.Pet;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

public class FamilyService {
    public FamilyDao db;


    FamilyService() {
        this.db = new CollectionFamilyDao();
    }

    FamilyService(FamilyDao db) {
        this.db = db;
    }

    public List<Family> getAllFamilies() {
        return db.getAllFamilies();
    }

    public void displayAllFamilies() {
        List<Family> f = db.getAllFamilies();
        f.forEach(System.out::println);
    }

    public void getFamiliesBiggerThan(int num) {
        List<Family> f = new ArrayList<>(db.getAllFamilies());
        System.out.printf("Families bigger than %d %n", num);
        f.forEach(x -> {
            if (x.countFamily() > num) {
                System.out.println(x);
            }
        });
    }

    public void getFamiliesLessThan(int num) {
        List<Family> f = new ArrayList<>(db.getAllFamilies());
        System.out.printf("Families less than %d %n", num);
        f.forEach(x -> {
            if (x.countFamily() < num) {
                System.out.println(x);
            }
        });
    }

    public int countFamiliesWithMemberNumber(int num) {
        AtomicInteger count = new AtomicInteger();
        List<Family> f = db.getAllFamilies();

        f.forEach(x -> {
            if (x.countFamily() == num) {
                count.getAndIncrement();
            }
        });

        return count.intValue();
    }

    public void createNewFamily(Human h1, Human h2) {
        Family f1 = new Family(h1, h2);
        db.saveFamily(f1);
    }

    public void deleteFamilyByIndex(int index) {
        db.deleteFamily(index);
    }

    public Family bornChild(Family f, String fatherName, String motherName) {
        f.bornChild(f, 2001);
        db.saveFamily(f);
        return f;
    }

    public Family adoptChild(Family f, Human h) {
        f.addChild(h);
        db.saveFamily(f);
        return f;
    }

    public void deleteAllChildrenOlderThen(int num){
        List<Family> f = db.getAllFamilies();
        f.forEach(x -> {
            x.getChildrens().removeIf(c -> new Date(c.getYear()).getYear() > num);
            db.saveFamily(x);
        });
    }

    public int count() {
        return db.getAllFamilies().size();
    }

    public Family getFamilyById(int index) {
        return db.getFamilyByIndex(index);
    }

    public Set<Pet> getPets(int index) {
        return db.getFamilyByIndex(index).getPets();
    }

    public void addPet(int index, Pet p) {
        Family f = db.getFamilyByIndex(index);
        f.addPet(p);
        db.saveFamily(f);
    }

}
