package HW10.Pets;

import java.util.Set;

public class Fish extends Pet {
    {
        super.setSpecies(Species.FISH);
    }
    @Override
    public void respond() {
        System.out.println("...");
    }

    public Fish() {
        super();
    }

    public Fish(String nickname) {
        super(nickname);
    }

    public Fish(String nickname, int age, int trickLevel, Set<String> habits){
        super(nickname, age, trickLevel, habits);
    }
}
