package HW10.Pets;

import java.util.Set;

public class Dog extends Pet implements PetFoul {
    {
        super.setSpecies(Species.DOG);
    }
    @Override
    public void respond() {
        System.out.println("Gav gav gav");
    }

    @Override
    public void foul() {
        System.out.println("I need to eat the sofa");
    }

    public Dog(){
        super();
    }

    public Dog(String nickname){
        super(nickname);
    }

    public Dog(String nickname, int age, int trickLevel, Set<String> habits){
        super(nickname, age, trickLevel, habits);
    }
}
