package HW10.Humans;


import HW10.Pets.Pet;

import java.util.Map;

public class Women extends Human {
    public Women() {
        super();
    }


    public Women(String name, String surname, long year) {
        super(name, surname, year);
    }


    public Women(String name, String surname, long year, int iq, Map<String, String> schedule) {
        super(name, surname, year, iq, schedule);
    }

    @Override
    public void greetPet() {
        System.out.printf("Hello from woman, %s !\n", ((Pet) super.getFamily().getPets().toArray()[0]).getNickname());
    }

    public void makeUp() {
        System.out.println("I have made a make up !!! I am beautiful !!!");
    }
}
