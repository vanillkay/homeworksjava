package HW1;

import java.util.Arrays;
import java.util.Optional;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

public class Game {

    final static Scanner input = new Scanner(System.in);

    public static void print(String s) {
        System.out.print(s);
    }

    public static int randomNum(int min, int max) {
        return ThreadLocalRandom.current().nextInt(min, max);
    }

    public static String askName() {
        print("Enter your name \n");
        return input.nextLine();
    }

    public static int askNum(String question, int min, int max) {
        print(String.format("%s \n", question));
        return checkStr(min, max);
    }



    public static Optional<Integer> isInt(String s) {
        try {
            int num = toInt(s);
            return Optional.of(num);
        } catch (NumberFormatException x) {
            return Optional.empty();
        }
    }

    public static int checkStr(int min, int max) {
        boolean check;
        int number;
        do {
            Optional<Integer> value = isInt(input.next());
            check = value.isEmpty();
            if (check) print("You entered a string, required a number. Try again !\n");
            if (value.isPresent()) {
                if (value.get() < min || value.get() > max){
                    check = true;
                    print(String.format("Entered the num between %d and %d !\n", min, max));
                }
            }
            number = value.orElse(0);
        } while (check);
        return number;
    }


    public static int toInt(String s) {
        return Integer.parseInt(s);
    }

    public static void bubbleSort1(int[] arr) {
        boolean isSorted = false;
        while (!isSorted) {
            isSorted = true;
            for (int i = 0; i < arr.length; i++) {
                if (i != arr.length - 1 && arr[i] < arr[i + 1]) {
                    int tmp = arr[i];
                    arr[i] = arr[i + 1];
                    arr[i + 1] = tmp;
                    isSorted = false;
                }
            }
        }
    }

    public static void bubbleSort(int[] arr) {
        boolean isSorted = false;
        while (!isSorted) {
            isSorted = true;
            for (int i = 0; i < arr.length; i++) {
                if (i != arr.length - 1) {
                    int num1 = arr[i];
                    int num2 = arr[i + 1];
                    if (num1 > num2) continue;
                    arr[i] = num2;
                    arr[i + 1] = num1;
                    isSorted = false;
                }
            }
        }
    }


    public static int[] pushNum(int[] arr, int num) {
        int[] newArr = Arrays.copyOf(arr, arr.length + 1);
        newArr[newArr.length - 1] = num;
        return newArr;
    }

    public static void game(int num, String name, String question) {
        int[] nums = new int[0];
        while (true) {
            int enteredNum = askNum(question, 1900, 2000);
            if (enteredNum < num) {
                print("Your number is too small. Please, try again.\n");
            } else if (enteredNum > num) {
                print("Your number is too big. Please, try again.\n");
            } else {
                print(String.format("Congratulations, %s ! Year is %d\n", name, num));
                bubbleSort(nums);
                print(String.format("Your nums: %s. \n", Arrays.toString(nums)));
                nums = new int[0];
            }
            if (enteredNum != num) {
                nums = pushNum(nums, enteredNum);
            }
        }
    }

    public static void main(String[] args) {

        Day[][] days = {
                {
                        new Day(1939, "Когда началась Вторая мировая война ?"),
                        new Day(1914, "Когда началась Первая мировая война ?"),
                        new Day(1947, "Когда началась Холодная война ?")
                },
                {
                        new Day(1917, "Когда началась Октябрьская революция ?"),
                        new Day(1963, "Когда было Убийство Джона Кеннеди ?"),
                        new Day(1941, "Когда началось Нападение на Перл-Харбор ?")
                },
                {
                        new Day(1941, "Когда начался Холокост ?"),
                        new Day(1944, "Когда была Высадка в Нормандии ?"),
                        new Day(1915, "Когда началась Дарданелльская операция ?")
                },
        };
        int x = randomNum(0, 2);
        int y = randomNum(0, 2);
        Day day = days[x][y];
        int date = day.date;
        String question = day.question;
        String name = askName();
        game(date, name, question);

    }

    public static class Day {
        int date;
        String question;

        public Day(int date, String question) {
            this.date = date;
            this.question = question;
        }
    }


}
