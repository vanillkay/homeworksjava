package test.HW6tests;

import HW7.Humans.Family;
import HW7.Humans.Human;
import HW7.Humans.Man;
import HW7.Humans.Women;
import HW7.Pets.DomesticCat;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class familyTests {
    static Women a = new Women("Mary", "Gofran", 2003, 23, new HashMap<String, String>());
    static Man b = new Man("Mike", "Leviren", 2001);
    static DomesticCat p2 = new DomesticCat("Vasya", 2, 43, new HashSet<String>() {{
        add("sleep");
        add("eat");
    }});


    static Family f = new Family(a, b, p2);


    @Test
    void humanToString() {
        String humanStr = "Human{name='Mike', surname='Leviren', year=2001, iq=0, schedule=[]}";
        assertEquals(humanStr, b.toString());
    }

    @Test
    void petTest(){
        String petStr = "DOMESTICCAT{nickname='Vasya', age=2, trickLevel=43, habits=[sleep, eat], can't fly, has 4 legs, has fur}";
        assertEquals(petStr, p2.toString());
    }





    @Test
    void countFamily(){
        //Test for family members
        Human child = f.bornChild(f, 2001);
        assertEquals(3, f.countFamily());

        f.deleteChild(0);
        assertEquals(2, f.countFamily());
    }

}
