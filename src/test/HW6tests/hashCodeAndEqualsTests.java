package test.HW6tests;

import HW3.Planner;
import HW5.*;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;




public class hashCodeAndEqualsTests {

    static String[][] schedule = Planner.createSchedule();
    static Human a = new Human("Mary", "Gofran", 2003, 23, schedule);
    static Human b = new Human("Mike", "Leviren", 2001);
    static Pet p = new Pet(Species.CAT, "Vasya", 13, 43, new String[]{"sleep", "eat"});

    static Family f = new Family(a, b, p);

    static Human c = new Human("Richard", "Leviren", 2004);
    static Human c1 = new Human("Richard", "Leviren", 2004);
    static Human c2 = new Human("Leo", "Leviren", 2004);


    @Test
    void equalsCheck(){
        assertEquals(c, c1);
        assertNotEquals(c, c2);
        assertNotEquals(c, f);
    }


    @Test
    void hashCodeCheck(){
        assertEquals(c.hashCode(), c1.hashCode());
        assertNotEquals(c.hashCode(), c2.hashCode());
    }
}
