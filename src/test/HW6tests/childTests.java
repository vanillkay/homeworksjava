package test.HW6tests;


import HW7.Humans.Family;

import HW7.Humans.Human;
import HW7.Pets.DomesticCat;
import HW7.Humans.Man;
import HW7.Humans.Women;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class childTests {
    static Women a = new Women("Mary", "Gofran", 2003, 23, new HashMap<String, String>());
    static Man b = new Man("Mike", "Leviren", 2001);
    static  DomesticCat p2 = new DomesticCat("Vasya", 2, 43, new HashSet<String>() {{
        add("sleep");
        add("eat");
    }});


    static Family f = new Family(a, b, p2);

    @Test
    void addChild(){
        Human child = f.bornChild(f, 2001);
        //Test after add child
        assertEquals(f.getChildrens().get(0), child);
    }


    @Test
    void deleteChild(){
        //Test if index is out of range
        assertFalse(f.deleteChild(2));

        //Test to check the childrens array
        assertEquals(f.getChildrens().size(), 1);

        //Test for deleting child
        assertTrue(f.deleteChild(0));

        assertArrayEquals(f.getChildrens().toArray(), new ArrayList<Human>().toArray());
    }
}
