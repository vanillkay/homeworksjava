package test.H8tests;

import HW8.Dao.FamilyController;
import HW8.Humans.Family;
import HW8.Humans.Man;
import HW8.Humans.Women;
import HW8.Pets.DomesticCat;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class FamilyServiceTests {

    static Women h1 = new Women("Mary", "Gofran", 2003, 23, new HashMap<>());
    static Man h2 = new Man("Mike", "Leviren", 2001);
    static DomesticCat p2 = new DomesticCat("Vasya", 2, 43, new HashSet<String>() {{
        add("sleep");
        add("eat");
    }});

    static Women h3 = new Women("Sophia", "Jart", 2003, 23, new HashMap<>());
    static Man h4 = new Man("Alex", "Grof", 2001);
    static DomesticCat p3 = new DomesticCat("Myrka", 2, 43, new HashSet<String>() {{
        add("play");
        add("eat");
    }});

    static FamilyController cont = new FamilyController();

    @BeforeEach
    void init() {
        cont.createNewFamily(h1, h2);
    }

    @AfterEach
    void init2() {
        cont.getAllFamilies().clear();
    }

    @Test
    void createNewFamily() {
        List<Family> testList = new ArrayList<>() {{
            add(new Family(h1, h2));
        }};
        assertArrayEquals(testList.toArray(), cont.getAllFamilies().toArray());
        cont.createNewFamily(h3, h4);
        testList.add(new Family(h3, h4));
        assertArrayEquals(testList.toArray(), cont.getAllFamilies().toArray());
    }


    @Test
    void countFamiliesWithMemberNumber() {
        cont.createNewFamily(h3, h4);
        assertEquals(cont.countFamiliesWithMemberNumber(2), 2);
    }

    @Test
    void bornChild() {
        Family f = cont.getFamilyById(0);
        cont.bornChild(f, f.getFather().getName(), f.getMother().getName());
        assertEquals(cont.getFamilyById(0).countFamily(), 3);
    }

    @Test
    void adoptChild() {
        Man h5 = new Man("Jake", "Miles", 2016);
        Family f = cont.getFamilyById(0);
        cont.adoptChild(f, h5);
        assertEquals(cont.getFamilyById(0).countFamily(), 3);
    }


    @Test
    void countFamilies() {
        assertEquals(1, cont.count());
        cont.createNewFamily(h3, h4);
        assertEquals(2, cont.count());
    }

    @Test
    void getFamilyById() {
        Family testF = new Family(h1, h2);
        assertEquals(cont.getFamilyById(0), testF);
    }

    @Test
    void getPets() {
        DomesticCat p2 = new DomesticCat("Vasya", 2, 43, new HashSet<>() {{
            add("sleep");
            add("eat");
        }});
        Family testF = new Family(h1, h2, p2);
        cont.addPet(0, p2);
        assertEquals(cont.getPets(0), testF.getPets());
    }


}


