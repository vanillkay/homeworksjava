package HW11.Dao;

import HW11.Humans.Family;

import java.util.ArrayList;
import java.util.List;

public class CollectionFamilyDao implements FamilyDao {

    static List<Family> list = new ArrayList<>();

    @Override
    public List<Family> getAllFamilies() {
        return list;
    }

    @Override
    public Family getFamilyByIndex(int index) {
        if (list.size() <= index) {
            return null;
        }
        return list.get(index);
    }

    @Override
    public boolean deleteFamily(int index) {
        if (list.size() <= index) {
            return false;
        }
        list.remove(index);
        return true;
    }

    @Override
    public boolean deleteFamily(Family f) {
        if (list.contains(f)) {
            list.remove(f);
            return true;
        }
        return false;
    }

    @Override
    public void saveFamily(Family f1) {
        list.removeIf(f -> f.getMother().getName().equals(f1.getMother().getName()) &&
                f.getFather().getName().equals(f1.getFather().getName()));
        list.add(f1);
    }
}
