package HW11;


import HW11.Dao.FamilyController;
import HW11.Humans.FamilyOverflowException;
import HW11.Humans.Man;
import HW11.Humans.Women;
import HW11.Pets.DomesticCat;

import java.util.HashMap;
import java.util.HashSet;

public class App {


    public static void main(String[] args) {


        Console console = new Console(new FamilyController());

        try {
            console.run();
        }catch (FamilyOverflowException e){
            System.out.println("You cant adopt or born child in family with 5 members !!!");
        }
    }
}
