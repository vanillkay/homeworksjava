package HW11.Humans;

import HW1.Game;

import java.util.HashMap;

public interface HumanCreator {



    public default Human bornChild(Family family, int year, String boyName, String girlName) {
        int num = Game.randomNum(1, 10);
        if (num > 5) {
            Man boy = new Man(boyName,
                    family.getFather().getSurname(), year, family.getFather().getIq() + family.getMother().getIq() / 2, new HashMap<>());
            boy.setFamily(family);
            family.addChild(boy);
            return boy;
        } else {
            Women girl = new Women(girlName,
                    family.getFather().getSurname(), year, family.getFather().getIq() + family.getMother().getIq() / 2, new HashMap<>());
            girl.setFamily(family);
            family.addChild(girl);
            return girl;
        }
    }

}
