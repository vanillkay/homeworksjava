package HW11.Humans;

public class FamilyOverflowException extends Exception{
    public FamilyOverflowException(String message){
        super(message);
    }
}
