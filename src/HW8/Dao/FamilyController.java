package HW8.Dao;

import HW8.Humans.Family;
import HW8.Humans.Human;
import HW8.Pets.Pet;

import java.util.List;
import java.util.Set;

public class FamilyController {
    FamilyService serv;

    public FamilyController(){
        serv = new FamilyService();
    }

    public List<Family> getAllFamilies() {
        return serv.getAllFamilies();
    }

    public void displayAllFamilies() {
        serv.displayAllFamilies();
    }

    public void getFamiliesBiggerThan(int num) {
        serv.getFamiliesBiggerThan(num);
    }

    public void getFamiliesLessThan(int num) {
        serv.getFamiliesLessThan(num);
    }

    public int countFamiliesWithMemberNumber(int num) {
        return serv.countFamiliesWithMemberNumber(num);
    }

    public void createNewFamily(Human h1, Human h2) {
        serv.createNewFamily(h1, h2);
    }

    public void deleteFamilyByIndex(int index) {
        serv.deleteFamilyByIndex(index);
    }

    public Family bornChild(Family f, String fatherName, String motherName) {
        return serv.bornChild(f, fatherName, motherName);
    }

    public Family adoptChild(Family f, Human h) {
        return serv.adoptChild(f, h);
    }

    public int count() {
        return serv.count();
    }

    public Family getFamilyById(int index) {
        return serv.getFamilyById(index);
    }

    public Set<Pet> getPets(int index) {
        return serv.getPets(index);
    }

    public void addPet(int index, Pet p) {
        serv.addPet(index, p);
    }
}
