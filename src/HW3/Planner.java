package HW3;

import HW5.dayOfWeek;

import java.util.Optional;
import java.util.Scanner;

public class Planner {

    final static Scanner input = new Scanner(System.in);


    public static String[][] createSchedule() {
        return new String[][]{
                {dayOfWeek.SUNDAY.name(), "do homework" },
                {dayOfWeek.MONDAY.name(), "go for a walk" },
                {dayOfWeek.TUESDAY.name(), "read a book" },
                {dayOfWeek.WEDNESDAY.name(), "write a letter for Jake" },
                {dayOfWeek.THURSDAY.name(), "repair my car" },
                {dayOfWeek.FRIDAY.name(), "meet with friends" },
                {dayOfWeek.SATURDAY.name(), "buy a new book" }
        };
    }

    public static Optional<String> findTask(String[][] schedule, String day) {
        for (String[] dayPlan : schedule) {
            if (dayPlan[0].equals(day)) {
                return Optional.of(dayPlan[1]);
            }
        }
        return Optional.empty();
    }

    public static void changeTask(String[][] schedule, String day, String newTask) {
        for (String[] dayPlan : schedule) {
            if (dayPlan[0].equals(day)) {
                dayPlan[1] = newTask;
            }
        }
    }

    public static boolean isChangingDay(String[][] schedule, String day) {
        boolean isChange = day.contains("change") || day.contains("reschedule");
        String dayStr = day.substring(day.indexOf(' ') + 1);
        Optional<String> isValidDay = findTask(schedule, dayStr);
        return isChange && isValidDay.isPresent();
    }


    public static void planner(String[][] schedule) {
        while (true) {

            System.out.println("Please, input the day of the week");
            String day = input.nextLine().toLowerCase().trim();

            switch (day) {
                case "monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday" -> {
                    {
                        String dayStr = String.format("%c%s", day.charAt(0) - 32, day.substring(1));
                        String task = findTask(schedule, day).orElse("No task for entered day");
                        System.out.printf("Your tasks for %s: %s%n", dayStr, task);
                    }
                }
                case "exit" -> {
                    return;
                }
                default -> {
                    if (isChangingDay(schedule, day)) {

                        String dayStr = day.substring(day.indexOf(' ') + 1);

                        String dayStrUp = String.format("%c%s", dayStr.charAt(0) - 32, dayStr.substring(1));

                        System.out.printf("Please, input new tasks for %s%n", dayStrUp);

                        String newTask = input.nextLine().toLowerCase().trim();

                        changeTask(schedule, dayStr, newTask);
                    } else {
                        System.out.println("Sorry, I don't understand you, please try again.");
                    }
                }
            }
        }
    }


    public static void main(String[] args) {
        String[][] schedule = createSchedule();
        planner(schedule);
    }
}
