package HW2;

import HW1.Game;

import java.util.StringJoiner;

public class seaBattle {

    public static String[][] createField(int size) {
        String[][] field = new String[size + 1][size + 1];
        for (int x = 0; x < field.length; x++) {
            for (int y = 0; y < field[x].length; y++) {
                if (x == 0 || y == 0) {
                    field[x][y] = String.format(" %d |", y == 0 ? x : y);
                } else {
                    field[x][y] = " - |";
                }
            }
        }
        return field;
    }

    public static void showField(String[][] field) {
        for (String[] strings : field) {
            System.out.println(arrToStr(strings));
        }
    }

    public static String arrToStr(String[] arr) {
        StringJoiner s = new StringJoiner("");
        for (String elem : arr) {
            s.add(elem);
        }
        return s.toString();
    }

    public static int[][] makeAim(int min, int max) {
        int[][] aim = new int[3][2];
        int startX = Game.randomNum(min, max + 1);
        int startY = Game.randomNum(min, max + 1);

        aim[0] = new int[]{startX, startY};

        int direction = Game.randomNum(0, 2);

        if (direction == 0) {
            if (startY == 1) {
                aim[1] = new int[]{startX, startY + 1};
                aim[2] = new int[]{startX, startY + 2};
            } else if (startY == 5) {
                aim[1] = new int[]{startX, startY - 1};
                aim[2] = new int[]{startX, startY - 2};
            } else {
                aim[1] = new int[]{startX, startY + 1};
                aim[2] = new int[]{startX, startY - 1};
            }
        } else {
            if (startX == 1) {
                aim[1] = new int[]{startX + 1, startY};
                aim[2] = new int[]{startX + 2, startY};
            } else if (startX == 5) {
                aim[1] = new int[]{startX - 1, startY};
                aim[2] = new int[]{startX - 2, startY};
            } else {
                aim[1] = new int[]{startX + 1, startY};
                aim[2] = new int[]{startX - 1, startY};
            }
        }

        return aim;
    }


    public static int[][] cutAim(int[][] aim, int cutPlace) {
        int[][] newAim = new int[aim.length - 1][2];

        int newAimPos = 0;
        for (int oldAimPart = 0; oldAimPart < aim.length; oldAimPart++) {
            if (oldAimPart == cutPlace) continue;
            newAim[newAimPos++] = aim[oldAimPart];
        }
        return newAim;
    }

    public static void game(String[][] field, int[][] aim) {

        int[][] gameAim = aim;

        boolean isHit = false;

        showField(field);

        while (gameAim.length != 0) {
            int enteredRow = Game.askNum("Entered the row", 1, 5);
            int enteredCol = Game.askNum("Entered the column", 1, 5);
            for (int x = 0; x < gameAim.length; x++) {
                if (gameAim[x][0] == enteredRow && gameAim[x][1] == enteredCol) {
                    field[enteredRow][enteredCol] = " X |";
                    gameAim = cutAim(gameAim, x);
                    isHit = true;
                    break;
                } else {
                    field[enteredRow][enteredCol] = " * |";
                    isHit = false;
                }
            }
            if (isHit){
                System.out.println("Nice shoot !");
            }else {
                System.out.println("Miss !");
            }
            showField(field);
        }
        System.out.println("\n You have won !");
    }


    public static void main(String[] args) {
//        String[][] field = createField(5);
//        int[][] aim = makeAim(1, 5);
//        System.out.println("All set. Get ready to rumble!");
//        game(field, aim);
    }
}
