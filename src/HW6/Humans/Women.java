package HW6.Humans;


public class Women extends Human {
    public Women() {
        super();
    }


    public Women(String name, String surname, int year) {
        super(name, surname, year);
    }


    public Women(String name, String surname, int year, int iq, String[][] schedule) {
        super(name, surname, year, iq, schedule);
    }

    @Override
    public void greetPet(){
        System.out.printf("Hello from woman, %s !\n", super.getFamily().getPet().getNickname());
    }

    public void makeUp(){
        System.out.println("I have made a make up !!! I am beautiful !!!");
    }
}
