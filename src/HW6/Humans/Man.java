package HW6.Humans;


public class Man extends Human {
    public Man() {
        super();
    }


    public Man(String name, String surname, int year) {
        super(name, surname, year);
    }


    public Man(String name, String surname, int year, int iq, String[][] schedule) {
        super(name, surname, year, iq, schedule);
    }

    @Override
    public void greetPet(){
        System.out.printf("Hello from man, %s !\n", super.getFamily().getPet().getNickname());
    }

    public void repairCar(){
        System.out.println("Ohhh no, i need o repair my car !");
    }
}
