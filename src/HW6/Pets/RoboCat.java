package HW6.Pets;

public class RoboCat extends Pet implements PetFoul {

    private final Species species = Species.FISH;

    @Override
    public void respond() {
        System.out.println("Mr eovvvv, * trrrr");
    }

    @Override
    public void foul() {
        System.out.println("I need to break the computer");
    }

    public RoboCat(){
        super();
    }

    public RoboCat(String nickname){
        super(nickname);
    }

    public RoboCat(String nickname, int age, int trickLevel, String[] habits){
        super(nickname, age, trickLevel, habits);
    }
}
