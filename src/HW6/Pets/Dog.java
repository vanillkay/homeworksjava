package HW6.Pets;

public class Dog extends Pet implements PetFoul {
    private final Species species = Species.DOG;
    @Override
    public void respond() {
        System.out.println("Gav gav gav");
    }

    @Override
    public void foul() {
        System.out.println("I need to eat the sofa");
    }

    public Dog(){
        super();
    }

    public Dog(String nickname){
        super(nickname);
    }

    public Dog(String nickname, int age, int trickLevel, String[] habits){
        super(nickname, age, trickLevel, habits);
    }
}
