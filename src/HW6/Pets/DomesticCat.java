package HW6.Pets;

import HW6.Pets.Pet;
import HW6.Pets.PetFoul;
import HW6.Pets.Species;

public class DomesticCat extends Pet implements PetFoul {

    private final Species species = Species.DOMESTICCAT;

    @Override
    public void respond() {
        System.out.println("Meov...mrrrrr");
    }

    @Override
    public void foul() {
        System.out.println("I need to bite my owner");
    }

    public DomesticCat(){
        super();
    }

    public DomesticCat(String nickname){
        super(nickname);
    }

    public DomesticCat(String nickname, int age, int trickLevel, String[] habits){
        super(nickname, age, trickLevel, habits);
    }
}
