package HW6.Pets;

import java.util.Arrays;

public abstract class Pet {
    private Species species = Species.UNKNOWN;
    private String nickname;
    private int age;
    private int trickLevel;
    private String[] habits;

    static {
        System.out.printf("New class is loading %s\n", Pet.class);
    }

    {
        System.out.printf("New Object is loading %s\n", Pet.class);
    }



    Pet(){
        nickname = "unknown";
        age = 0;
        trickLevel = 0;
        habits = new String[0];
    }




    Pet(String nickname){
        this();
        this.nickname = nickname;
    }

    public Pet(String nickname, int age, int trickLevel, String[] habits){
        this(nickname);
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }


    public void eat() {
        System.out.println("I'm eating !");
    }

    public abstract void respond();


    public Species getSpecies() {
        return species;
    }

    public void setSpecies(Species species) {
        this.species = species;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public String[] getHabits() {
        return habits;
    }

    public void setHabits(String[] habits) {
        this.habits = habits;
    }

    @Override
    public String toString() {
        return String.format("%s{nickname='%s', age=%d, trickLevel=%d, habits=%s, %s, has %s legs, %s}",
                species.name(),
                nickname,
                age,
                trickLevel,
                Arrays.toString(habits),
                species.isCanFly() ? "can fly" : "can't fly",
                species.getNumberOfLegs(),
                species.isHasFur() ? "has fur" : "hasn't fur"
        );
    }


    @Override
    public boolean equals(Object obj){
        if (this == obj)
            return true;
        if (!(obj instanceof Pet))
            return false;
        Pet pet = (Pet) obj;
        if(this.age != pet.age || this.trickLevel != pet.trickLevel) return false;
        return this.species.equals(pet.species) && this.nickname.equals(pet.nickname);
    }

    @Override
    public int hashCode(){
        int code = 10;
        int k = 5;
        code = k*code + age + trickLevel;
        return code;
    }

    @Override
    public void finalize(){
        System.out.println(this);
    }
}
