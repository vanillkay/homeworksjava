package HW6.Pets;

public class Fish extends Pet {
    private final Species species = Species.FISH;

    @Override
    public void respond() {
        System.out.println("...");
    }

    public Fish() {
        super();
    }

    public Fish(String nickname) {
        super(nickname);
    }

    public Fish(String nickname, int age, int trickLevel, String[] habits){
        super(nickname, age, trickLevel, habits);
    }
}
