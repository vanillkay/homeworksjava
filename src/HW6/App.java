package HW6;


import HW3.Planner;

import HW6.Humans.Family;
import HW6.Humans.Man;
import HW6.Humans.Women;
import HW6.Pets.DomesticCat;

public class App {


    public static void main(String[] args) {

        String[][] schedule = Planner.createSchedule();

        Women h1 = new Women("Mary", "Gofran", 2003, 23, schedule);
        Man h2 = new Man("Mike", "Leviren", 2001);
        DomesticCat p2 = new DomesticCat("Vasya", 2, 43, new String[]{"sleep", "eat"});

        Family f1 = new Family(h1, h2, p2);

        System.out.println(f1);
        f1.bornChild(f1, 2021);
        System.out.println(f1);
        h1.greetPet();
    }
}
