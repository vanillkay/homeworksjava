package HW12.Dao;

import HW12.Humans.Family;
import HW12.Humans.Human;
import HW12.Pets.Pet;

import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

public class FamilyService {
    public FamilyDao db;
    private final File dbDir = new File("db");
    private final File dbFile = new File("db/db.bin");


    FamilyService() {
        this.db = new CollectionFamilyDao();
    }

    public List<Family> getAllFamilies() {
        return db.getAllFamilies();
    }


    public void initializeDb() throws IOException, ClassNotFoundException {
        boolean mkdir = dbDir.mkdir();
        if (mkdir) {
            boolean newFile = dbFile.createNewFile();
            writeFamilies();
        } else {
            if (!dbFile.exists()) {
                boolean newFile = dbFile.createNewFile();
                writeFamilies();
            }
        }
    }

    public void readFamilies() throws IOException, ClassNotFoundException {
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream(dbFile));
        db.changeFamilies((ArrayList<Family>) ois.readObject());
        ois.close();
    }

    private void writeFamilies() throws IOException {
        dbFile.delete();
        dbFile.createNewFile();
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(dbFile))) {
            oos.writeObject(getAllFamilies());
        }
    }

    public void displayAllFamilies() {
        List<Family> f = db.getAllFamilies();
        f.forEach(x -> System.out.println(x.prettyFormat()));
    }

    public void getFamiliesBiggerThan(int num) {
        List<Family> f = new ArrayList<>(db.getAllFamilies());
        System.out.printf("Families bigger than %d %n", num);
        f.forEach(x -> {
            if (x.countFamily() > num) {
                System.out.println(x.prettyFormat());
            }
        });
    }

    public void getFamiliesLessThan(int num) {
        List<Family> f = new ArrayList<>(db.getAllFamilies());
        System.out.printf("Families less than %d %n", num);
        f.forEach(x -> {
            if (x.countFamily() < num) {
                System.out.println(x.prettyFormat());
            }
        });
    }

    public int countFamiliesWithMemberNumber(int num) {
        AtomicInteger count = new AtomicInteger();
        List<Family> f = db.getAllFamilies();

        f.forEach(x -> {
            if (x.countFamily() == num) {
                count.getAndIncrement();
            }
        });

        return count.intValue();
    }

    public void createNewFamily(Human h1, Human h2) throws IOException {
        Family f1 = new Family(h1, h2);
        db.saveFamily(f1);
        writeFamilies();
    }

    public void deleteFamilyByIndex(int index) throws IOException {
        db.deleteFamily(index);
        writeFamilies();
    }

    public Family bornChild(Family f, String fatherName, String motherName) throws IOException {
        f.bornChild(f, 2001, fatherName, motherName);
        db.saveFamily(f);
        writeFamilies();
        return f;
    }

    public Family adoptChild(Family f, Human h) throws IOException {
        f.addChild(h);
        db.saveFamily(f);
        writeFamilies();
        return f;
    }

    public void deleteAllChildrenOlderThen(int num) throws IOException {
        List<Family> f = db.getAllFamilies();
        f.forEach(x -> {
            x.getChildrens().removeIf(c -> new Date(c.getYear()).getYear() > num);
            db.saveFamily(x);
        });
        writeFamilies();
    }

    public int count() {
        return db.getAllFamilies().size();
    }

    public Family getFamilyById(int index) {
        return db.getFamilyByIndex(index);
    }

    public Set<Pet> getPets(int index) {
        return db.getFamilyByIndex(index).getPets();
    }

    public void addPet(int index, Pet p) throws IOException {
        Family f = db.getFamilyByIndex(index);
        f.addPet(p);
        db.saveFamily(f);
        writeFamilies();
    }

}
