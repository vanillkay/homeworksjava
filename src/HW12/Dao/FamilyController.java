package HW12.Dao;

import HW12.Humans.Family;
import HW12.Humans.Human;
import HW12.Pets.Pet;

import java.io.IOException;
import java.util.List;
import java.util.Set;

public class FamilyController {
    FamilyService serv = new FamilyService();

    {
        try {
            serv.initializeDb();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            System.out.println("Something went wrong enter the action again !");
        }
    }

    public void readFamilies() throws IOException, ClassNotFoundException {
        serv.readFamilies();
    }

    public List<Family> getAllFamilies() {
        return serv.getAllFamilies();
    }

    public void displayAllFamilies() {
        serv.displayAllFamilies();
    }

    public void getFamiliesBiggerThan(int num) {
        serv.getFamiliesBiggerThan(num);
    }

    public void getFamiliesLessThan(int num) {
        serv.getFamiliesLessThan(num);
    }

    public int countFamiliesWithMemberNumber(int num) {
        return serv.countFamiliesWithMemberNumber(num);
    }

    public void createNewFamily(Human h1, Human h2) throws IOException {
        serv.createNewFamily(h1, h2);
    }

    public void deleteFamilyByIndex(int index) throws IOException {
        serv.deleteFamilyByIndex(index);
    }

    public Family bornChild(Family f, String fatherName, String motherName) throws IOException {
        return serv.bornChild(f, fatherName, motherName);
    }

    public Family adoptChild(Family f, Human h) throws IOException {
        return serv.adoptChild(f, h);
    }

    public int count() {
        return serv.count();
    }

    public Family getFamilyById(int index) {
        return serv.getFamilyById(index);
    }

    public Set<Pet> getPets(int index) {
        return serv.getPets(index);
    }

    public void addPet(int index, Pet p) throws IOException {
        serv.addPet(index, p);
    }
}
