package HW12.Dao;

import HW12.Humans.Family;

import java.util.List;

public interface FamilyDao {
    public List<Family> getAllFamilies();

    public void changeFamilies(List<Family> newFamilies);

    public Family getFamilyByIndex(int index);

    public boolean deleteFamily(int index);

    public boolean deleteFamily(Family f);

    public void saveFamily(Family f);




}
