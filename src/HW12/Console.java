package HW12;

import HW12.Dao.FamilyController;
import HW12.Humans.*;
import HW12.Pets.DomesticCat;

import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class Console {
    final private FamilyController controller;
    final private Scanner scanner = new Scanner(System.in);
    final private Logger logger = new Logger();
    final private Map<Integer, Runnable> options = new HashMap<>() {{
        put(1, Console.this::initialize);
        put(2, Console.this::showAllFamilies);
        put(3, Console.this::showSomeFamiliesBiggerThan);
        put(4, Console.this::showSomeFamiliesLessThan);
        put(5, Console.this::countSomeFamiliesWithNumber);
        put(6, Console.this::createNewFamily);
        put(7, Console.this::deleteFamily);
        put(8, Console.this::updateFamily);
        put(9, Console.this::deleteChildrenOlderThan);
    }};

    public Console(FamilyController controller) {
        this.controller = controller;
    }

    public void initialize() {
        try {
            controller.readFamilies();
            logger.info("Initialize the db");
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            System.out.println("Something went wrong try again");
            logger.error("Error in initialization db");
        }
    }

    private int askNum(String question, int min, int max) {
        System.out.printf("%s \n", question);
        return checkStr(min, max);
    }


    private static int toInt(String s) {
        return Integer.parseInt(s);
    }

    private static Optional<Integer> isInt(String s) {
        try {
            int num = toInt(s);
            return Optional.of(num);
        } catch (NumberFormatException x) {
            return Optional.empty();
        }
    }

    private int checkStr(int min, int max) {
        boolean check;
        int number;
        do {
            Optional<Integer> value = isInt(scanner.next());
            check = value.isEmpty();
            if (check) System.out.println("You entered a string, required a number. Try again !");
            if (value.isPresent()) {
                if (value.get() < min || value.get() > max) {
                    check = true;
                    System.out.printf("Entered the num between %d and %d !\n", min, max);
                }
            }
            number = value.orElse(0);
        } while (check);
        return number;
    }

    public void showAllFamilies() {
        controller.getAllFamilies().forEach(x -> System.out.println(x.prettyFormat()));
        logger.info("Show all families");
    }

    public void showSomeFamiliesBiggerThan() {
        int number = askNum("Entered the num", 0, 100);
        controller.getFamiliesBiggerThan(number);
        logger.info(String.format("Show families bigger than %d", number));
    }

    public void showSomeFamiliesLessThan() {
        int number = askNum("Entered the num", 0, 100);
        controller.getFamiliesLessThan(number);
        logger.info(String.format("Show families less than %d", number));
    }

    public void countSomeFamiliesWithNumber() {
        int number = askNum("Entered the num", 0, 100);
        AtomicInteger count = new AtomicInteger();
        controller.getAllFamilies().forEach(x -> {
            if (x.countFamily() == number) count.getAndIncrement();
        });
        System.out.printf("Families with %d members -> %d\n", number, count.intValue());
        logger.info(String.format("Count families with %d members", number));
    }

    public String askString(String question) {
        System.out.println(question);
        boolean isString = true;
        String string;
        do {
            isString = true;
            string = scanner.next();
            for (int i = 0; i < string.length(); i++) {
                char letter = string.charAt(i);
                if (!Character.isLetter(letter)) {
                    System.out.println("Invalid value ! Enter a string !");
                    isString = false;
                    break;
                }
            }
        } while (!isString);
        return string;
    }

    public void createNewFamily() {
        String motherName = askString("Enter the mother's name");
        String motherSurName = askString("Enter the mother's surname");
        int motherYear = askNum("Enter the mother's year of birth", 1900, 2015);
        int motherMonth = askNum("Enter the mother's month of birth", 1, 12);
        int motherDay = askNum("Enter the mother's day of birth", 1, 31);
        int motherIQ = askNum("Enter the mother's IQ", 1, 100);

        LocalDate motherBirth = LocalDate.of(motherYear, motherMonth, motherDay);
        long motherBirthTimestamp = motherBirth.atStartOfDay(ZoneId.systemDefault()).toEpochSecond();
        Women mother = new Women(motherName, motherSurName, motherBirthTimestamp, motherIQ, new HashMap<>());

        String fatherName = askString("Enter the father's name");
        String fatherSurName = askString("Enter the father's surname");
        int fatherYear = askNum("Enter the father's year of birth", 1900, 2015);
        int fatherMonth = askNum("Enter the father's month of birth", 1, 12);
        int fatherDay = askNum("Enter the father's day of birth", 1, 31);
        int fatherIQ = askNum("Enter the father's IQ", 1, 100);

        LocalDate fatherBirth = LocalDate.of(fatherYear, fatherMonth, fatherDay);
        long fatherBirtTimestamp = fatherBirth.atStartOfDay(ZoneId.systemDefault()).toEpochSecond();
        Man father = new Man(fatherName, fatherSurName, fatherBirtTimestamp, fatherIQ, new HashMap<>());

        try {
            controller.createNewFamily(mother, father);
            logger.info("Created new family");
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Something went wrong try again");
            logger.error("Error with creating new family");
        }
        System.out.println("Your family was created !");

    }

    public void deleteFamily() {
        int index = askNum("Enter the family index", 0, controller.getAllFamilies().size());
        try {
            controller.deleteFamilyByIndex(index);
            logger.info("Deleted family");
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Something went wrong try again");
            logger.error("Error in deleting family");
        }

    }

    public void updateFamily() {
        int index = askNum("Enter the family index", 0, controller.getAllFamilies().size());
        System.out.println("1. Родить ребенка");
        System.out.println("2. Усыновить ребенка");
        System.out.println("3. Вернуться в главное меню");
        System.out.print(">>> ");
        int action = askNum("Enter the action", 1, 3);
        Family f = controller.getFamilyById(index);
        try {
            switch (action) {
                case 1: {
                    if (f.countFamily() == 5)
                        throw new FamilyOverflowException("Your family contains 5 members, you can't born child");

                    String boyName = askString("Enter the boy's name");
                    String girlName = askString("Enter the girl's name");
                    f.bornChild(f, 2021, boyName, girlName);
                    logger.info("Born child");
                    break;
                }
                case 2: {
                    if (f.countFamily() == 5)
                        throw new FamilyOverflowException("Your family contains 5 members, you can't adopt child");
                    System.out.println("1. Boy");
                    System.out.println("2. Girl");
                    int humanType = askNum("Enter the type", 1, 2);
                    String childName = askString("Enter the child's name");
                    String childSurname = askString("Enter the child's surname");
                    int childYear = askNum("Enter the child's year of birth", 1900, 2015);
                    int childMonth = askNum("Enter the child's month of birth", 1, 12);
                    int childDay = askNum("Enter the child's day of birth", 1, 31);
                    int childIQ = askNum("Enter the child's IQ", 1, 100);
                    LocalDate childBirth = LocalDate.of(childYear, childMonth, childDay);
                    long childBirthTimestamp = childBirth.atStartOfDay(ZoneId.systemDefault()).toEpochSecond();
                    Human child = humanType == 1 ? new Man(childName, childSurname, childBirthTimestamp, childIQ, new HashMap<>()) :
                            new Women(childName, childSurname, childBirthTimestamp, childIQ, new HashMap<>());
                    try {
                        controller.adoptChild(f, child);
                    } catch (IOException e) {
                        e.printStackTrace();
                        System.out.println("Something went wrong try again");
                    }
                    logger.info("Adopt child");
                    break;
                }
                case 3:
                default:
                    break;
            }
        } catch (FamilyOverflowException e) {
            System.out.println("You cant adopt or born child in family with 5 members !!!");
            logger.error("Try to born or adopt child in family with 5 members");
        }
    }

    public void deleteChildrenOlderThan() {
        int age = askNum("Enter the age", 0, 100);
        controller.getAllFamilies().forEach(x -> x.getChildrens().removeIf(y -> y.getAge() > age));
        logger.info(String.format("Deleted children older than %d", age));
    }

    public void showOptions() {
        System.out.println("------------------------------------------------------------------------");
        System.out.println("1. Прочитать данные");
        System.out.println("2. Отобразить весь список семей");
        System.out.println("3. Отобразить список семей, где количество людей больше заданного");
        System.out.println("4. Отобразить список семей, где количество людей меньше заданного");
        System.out.println("5. Подсчитать количество семей, где количество членов равно");
        System.out.println("6. Создать новую семью");
        System.out.println("7. Удалить семью по индексу семьи в общем списке");
        System.out.println("8. Редактировать семью по индексу семьи в общем списке");
        System.out.println("9. Удалить всех детей старше возраста");
        System.out.println("10. Выйти");
    }

    public void run() {
        while (true) {
            showOptions();
            int operation = askNum("\nEnter operation", 1, 10);
            if (operation == 10) break;
            options.get(operation).run();
        }
    }
}



