package HW12;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

public class Logger {

    private final File dbDir = new File("db");
    private final File dbFile = new File("db/application.log");

    {
        try {
            boolean mkdir = dbDir.mkdir();
            if (mkdir) {
                boolean newFile = dbFile.createNewFile();
            } else {
                if (!dbFile.exists()) {
                    boolean newFile = dbFile.createNewFile();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void info(String info)  {
        String log = String.format("INFO | %s, %s", LocalDateTime.now().format(DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL)), info);
        writeLog(log);
    }

    public void error(String error) {
        String log = String.format("ERROR | %s, %s", LocalDateTime.now().format(DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL)), error);
        writeLog(log);
    }

    private void writeLog(String log) {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(dbFile, true))) {
            bw.append(log);
            bw.append(System.getProperty("line.separator"));
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Error in logging !");
        }
    }
}
