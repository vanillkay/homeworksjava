package HW4;

import HW3.Planner;

public class App {


    public static void main(String[] args) {
        String[][] schedule = Planner.createSchedule();
        Human h1 = new Human("Jane", "Fery", 2003, 23, schedule);
        Human h2 = new Human("Jake", "Rocford", 2001);
        Family f1 = new Family(h1, h2);

        System.out.println(h1);
        System.out.println(h2);
        System.out.println(f1);
        System.out.println("-----");


        //----------------------------------------------
        Human h3 = new Human("Mary", "Gofran", 2003, 23, schedule);
        Human h4 = new Human("Mike", "Leviren", 2001);
        Pet p1 = new Pet("dog", "Simba");

        Family f2 = new Family(h3, h4, p1);

        System.out.println(h3);
        System.out.println(h4);
        System.out.println(p1);
        System.out.println(f2);
        System.out.println("-----");


        //----------------------------------------------
        Human h5 = new Human("Mary", "Gofran", 2003, 23, schedule);
        Human h6 = new Human("Mike", "Leviren", 2001);
        Pet p2 = new Pet("cat", "Vasya", 2, 43, new String[]{"sleep", "eat"});

        Family f3 = new Family(h5, h6, p2);

        System.out.printf("The family includes %d members%n", f3.countFamily());

        //Add and delete child
        Human h7 = new Human("Richard", "Leviren", 2004);
        f3.addChild(h7);;
        System.out.printf("The family includes %d members%n", f3.countFamily());
        System.out.println("Add and delete child");
        System.out.println(f3);
        System.out.println(h7.getFamily());
        System.out.println(h7);
        h7.greedPet();
        h7.describePet();
        h7.getFamily().getPet().eat();
        h7.getFamily().getPet().respond();
        h7.getFamily().getPet().foul();
        h7.feedPat(false);
        f3.deleteChild(0);
        System.out.println(f3);
        System.out.printf("The family includes %d members%n", f3.countFamily());
    }
}
